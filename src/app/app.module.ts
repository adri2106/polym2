import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { NavbarComponent } from './shared/navbar/navbar.component';
import { HomeComponent } from './components/home/home.component';
import { EstadisticasComponent } from './components/estadisticas/estadisticas.component';
import { PlantillaComponent } from './components/plantilla/plantilla.component';
import { EdicionComponent } from './components/edicion/edicion.component';
import { FooterComponent } from './shared/footer/footer.component';
import { AppRoutingModule } from './routes';
import { FormsModule,  ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { IdentificadoresPipe } from './pipes/identificadores.pipe';
import { DetalleComponent } from './components/detalle/detalle.component';
import { HistoriaComponent } from './components/historia/historia.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    HomeComponent,
    EstadisticasComponent,
    PlantillaComponent,
    EdicionComponent,
    FooterComponent,
    IdentificadoresPipe,
    DetalleComponent,
    HistoriaComponent,
    
  ],
  imports: [
    BrowserModule, AppRoutingModule, FormsModule, ReactiveFormsModule, HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
