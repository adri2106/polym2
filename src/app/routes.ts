import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { EstadisticasComponent } from './components/estadisticas/estadisticas.component';
import { PlantillaComponent } from './components/plantilla/plantilla.component';
import { EdicionComponent } from './components/edicion/edicion.component';
import { DetalleComponent } from './components/detalle/detalle.component';
import { HistoriaComponent } from './components/historia/historia.component';

const routes: Routes = [
    { path: '', component: HomeComponent },
    { path: 'home', component:HomeComponent },
    { path: 'historia', component:HistoriaComponent },

    { path: 'estadisticas', component:EstadisticasComponent },
    { path: 'plantilla', component:PlantillaComponent },
    { path: 'edicion/:id', component:EdicionComponent },
    { path: 'detalle/:id', component:DetalleComponent },
    { path: 'home', component:HomeComponent },



    { path: '**', component: HomeComponent },
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {}
