import { Component, OnInit } from "@angular/core";
import { Validators, FormGroup, FormControl } from "@angular/forms";
import { Router, ActivatedRoute } from "@angular/router";
import { PolymfbService } from "src/app/services/polymfb.service";
import { first } from "rxjs/operators"

@Component({
  selector: "app-edicion",
  templateUrl: "./edicion.component.html",
  styles: []
})
export class EdicionComponent implements OnInit {
  public jugador: any = [];

  public pForma: FormGroup;
  constructor(
    private pS: PolymfbService,
    private router: Router,
    private actRoute: ActivatedRoute
  ) {
    this.pForma = new FormGroup({
      nombre: new FormControl("", Validators.required),
      dorsal: new FormControl("", Validators.required),
      posicion: new FormControl("", Validators.required),
      comentarios: new FormControl(),
      goles: new FormControl(),
      ta: new FormControl(),
      imagen: new FormControl(),
      id:new FormControl()
    });
  }

  cancelarEnvio(ev) {
    ev.preventDefault();
  }
  guardar() {
    this.jugador = this.pForma.value;
    // console.log(this.jugador);
    // if(this.actRoute.snapshot.params.id ==="new"){
    //         this.nuevo();}
    //   else{
    //     this.actualizar(this.actRoute.snapshot.params.id)
    //   }
    
    this.actRoute.params.subscribe(param => {
           this.jugador.id=param.id;
      if (param.id === "new") {
        this.nuevo();
       
       
      } else {
        this.actualizar(param.id);
      }
    });
  };

  nuevo() {
    this.pS.crear(this.jugador).subscribe((resultado: any) => {
      console.log(resultado);
     
   
      this.router.navigate(["/edicion", resultado.name]);
    });
  }
  actualizar(id: string) {
    this.pS.actualizar(this.jugador, id).subscribe(resultado => {
      console.log(resultado);
    });
  }

  ngOnInit() {
    if (this.actRoute.snapshot.params.id !== "new") {
      const id = this.actRoute.snapshot.params.id;
      this.pS.verUno(id).subscribe(res => {
        console.log(res);
        if (res === null) {
          this.router.navigate(["/edicion", "new"]);
        } else {
          this.pForma.setValue(res);
        }
      });
    }
  }
}
