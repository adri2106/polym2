import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PolymfbService } from 'src/app/services/polymfb.service';

@Component({
  selector: 'app-detalle',
  templateUrl: './detalle.component.html',
  styles: []
})
export class DetalleComponent implements OnInit {
public jugador:any
  constructor(private activatedRoute:ActivatedRoute,
    private pS: PolymfbService,
    private router:Router) { 

    const id = this.activatedRoute.snapshot.params.id;
    this.pS.verUno(id).subscribe(data=>{
      this.jugador= data;
    });

    }
  

  ngOnInit() {
  }

}
