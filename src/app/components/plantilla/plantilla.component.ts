import { Component, OnInit } from '@angular/core';
import { PolymfbService } from 'src/app/services/polymfb.service';

@Component({
  selector: 'app-plantilla',
  templateUrl: './plantilla.component.html',
  styles: []
})
export class PlantillaComponent implements OnInit {

  constructor(private pS :PolymfbService) { this.cargarJugadores()}
public jugadores: any = [];
 cargarJugadores(){
    this.pS.verTodos().subscribe(data=>{
   this.jugadores = data || {};
 })}


  ngOnInit() {
  }
}

