import { Component, OnInit } from "@angular/core";
import { PolymfbService } from "src/app/services/polymfb.service";

@Component({
  selector: "app-estadisticas",
  templateUrl: "./estadisticas.component.html",
  styles: []
})
export class EstadisticasComponent implements OnInit {
  public jugadores: any = [];
  constructor(private pS: PolymfbService) {
    this.cargarJugadores();
  }
  cargarJugadores() {
    this.pS.verTodos().subscribe(resultado => {
      this.jugadores = resultado || {};
      console.log(this.jugadores);
    });
  }
  eliminarJugador(id: string) {
    if(confirm('¿Seguro?')){
    this.pS.eliminar(id).subscribe(res=> {
      if (res === null) {
        delete this.jugadores[id];
        this.cargarJugadores();
      }
    });
  }}

  ngOnInit() {
    
  }
}
