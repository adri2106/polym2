import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ficha} from './../ficha';

@Injectable({
  providedIn: 'root'
})
export class PolymfbService {

  private url = 'https://polym-c86db.firebaseio.com/jugadores.json';
  private urlJ = 'https://polym-c86db.firebaseio.com/jugadores';
  constructor(private http: HttpClient) { }

crear(j:ficha){
  return this.http.post(this.url,j)
}
verTodos(){
  
  return this.http.get(this.url)
}
actualizar(j:ficha, id:string){
 const urlF = `${this.urlJ}/${id}.json`
 return this.http.put(urlF, j)
}
verUno(id: string) {
  const url = `${this.urlJ}/${id}.json`;
  return this.http.get(url);
}
eliminar(id:string){
const urlF=`${this.urlJ}/${id}.json`
return this.http.delete(urlF)
}
}
